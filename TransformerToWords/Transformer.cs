﻿using System;
using System.Globalization;

namespace TransformerToWords
{
    public class Transformer
    {
        public static string TransformOneWord(double number)
        {
            if (double.IsNaN(number))
            {
                return "Not a Number";
            }

            if (double.IsNegativeInfinity(number))
            {
                return "Negative Infinity";
            }

            if (double.IsPositiveInfinity(number))
            {
                return "Positive Infinity";
            }

            if (number == double.Epsilon)
            {
                return "Double Epsilon";
            }

            string strValue = number.ToString(CultureInfo.InvariantCulture);
            char[] numberArray = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'E', '-', '+', '.' };
            string[] convertArray = new string[] { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "E", "minus", "plus", "point" };
            string wordFormat = string.Empty;

            for (int i = 0; i < strValue.Length; i++)
            {
                for (int j = 0; j < convertArray.Length; j++)
                {
                    if (strValue[i] == numberArray[j])
                    {
                        wordFormat = string.Concat(wordFormat, ' ', convertArray[j]);
                        break;
                    }
                }
            }

            wordFormat = wordFormat.Trim();
            return string.Concat(char.ToUpper(wordFormat[0], CultureInfo.CurrentCulture), wordFormat[1..]);
        }

#pragma warning disable CA1822
        public string[] Transform(double[] source)
#pragma warning restore CA1822
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (source.Length == 0)
            {
                throw new ArgumentException("Source is empty.");
            }

            string[] resultArray = new string[source.Length];

            for (int i = 0; i < source.Length; i++)
            {
                resultArray[i] = TransformOneWord(source[i]);
            }

            return resultArray;
        }   
    }
}
